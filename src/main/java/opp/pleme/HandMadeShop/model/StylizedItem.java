package opp.pleme.HandMadeShop.model;

import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
public class StylizedItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private float priceOfStylizedItem;

    @NotNull
   @Cascade(value = org.hibernate.annotations.CascadeType.MERGE)
    @ManyToOne
    @JoinColumn(name = "napkin_id")
    private Napkin napkin;

    @NotNull
    @Cascade(value = org.hibernate.annotations.CascadeType.MERGE)
    @ManyToOne
    @JoinColumn(name = "item_id")
    private Item item;

    @NotNull
    @Cascade(value = org.hibernate.annotations.CascadeType.MERGE)
    @ManyToOne
    @JoinColumn(name = "style_id")
    private Style style;

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    @Column(name="opis")
    private String opis;

    @ManyToOne
    @JoinColumn(name = "shoppingCart_id")
    private ShoppingCart shoppingCart;

    public StylizedItem(){}
    public ShoppingCart getShoppingCart() {
        return shoppingCart;
    }

    public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }

    public StylizedItem(Napkin napkin, Item item, Style style) {
        this.napkin = napkin;
        this.item = item;
        this.style = style;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public float getPriceOfStylizedItem() {

        return priceOfStylizedItem;
    }

    public void setPriceOfStylizedItem(float priceOfStylizedItem) {

        this.priceOfStylizedItem = priceOfStylizedItem;
    }

    public Napkin getNapkin() {
        return napkin;
    }

    public void setNapkin(Napkin napkin) {
        this.napkin = napkin;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public Style getStyle() {
        return style;
    }

    public void setStyle(Style style) {
        this.style = style;
    }

    public float izracunajCijenu(Style stil, Item item){
        float cijena =0;
        cijena= item.getItemPrice()*stil.getCoefficient();
        return cijena;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StylizedItem that = (StylizedItem) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "StylizedItem{" +
                "id=" + id +
                ", priceOfStylizedItem=" + priceOfStylizedItem +
                ", napkin=" + napkin +
                ", item=" + item +
                ", style=" + style +
                '}';
    }
}
