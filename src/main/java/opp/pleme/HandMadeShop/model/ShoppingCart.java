package opp.pleme.HandMadeShop.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "shopping_cart")
public class ShoppingCart {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "paid")
    private boolean paid = false;

    @Column(name = "total_cost")
    private float totalCost = 0;

    @Column(name = "username")
    private  String username;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "shoppingCart")
    private List<StylizedItem> items = new ArrayList<>();


    @OneToOne
    @JoinColumn(name = "form_id")
    private Form form;

    public ShoppingCart(){

    }
    public void addItem(StylizedItem stylizedItem, float cost){
        setTotalCost(cost+getTotalCost());
        items.add(stylizedItem);
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isPaid() {
        return paid;
    }

    public void setPaid(boolean paid) {
        this.paid = paid;
    }

    public float getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(float totalCost) {
        this.totalCost = totalCost;
    }


    public List<StylizedItem> getItems() {
        return items;
    }

    public void setItems(List<StylizedItem> items) {
        this.items = items;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingCart that = (ShoppingCart) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ShoppingCart{" +
                "id=" + id +
                ", paid=" + paid +
                ", totalCost=" + totalCost +
                ", items=" + items +
                ", form=" + form +
                '}';
    }
}
