package opp.pleme.HandMadeShop.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "napkins")
public class Napkin {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

  //  @NotNull
    @Column(name = "name")
    private String name;

    public Napkin(){}
    public Napkin(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Napkin napkin = (Napkin) o;
        return Objects.equals(id, napkin.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Napkin{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
