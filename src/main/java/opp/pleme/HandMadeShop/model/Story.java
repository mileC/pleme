package opp.pleme.HandMadeShop.model;

import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "stories")
public class Story {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "title")
    private String title;

    @Column(name = "video")
    private String video;

    @Column(name = "picture")
    private String picture;

    @Column(name = "text", length = 10000)
    private String text;

    @OneToMany(cascade = CascadeType.MERGE, mappedBy = "story")
    private List<Comment> comments = new ArrayList<>();

    @Column(name = "aprooved")
    private boolean approved;

    public Story() {

    }

    public Story(String title, String text, String video, String picture, boolean approved) {
        this.title = title;
        this.text = text;
        this.video = video;
        this.picture = picture;
        this.approved = approved;
    }


    public Story(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Story story = (Story) o;
        return Objects.equals(id, story.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Story{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", video='" + video + '\'' +
                ", picture='" + picture + '\'' +
                ", text='" + text + '\'' +
                ", comments=" + comments +
                ", approved=" + approved +
                '}';
    }
}

