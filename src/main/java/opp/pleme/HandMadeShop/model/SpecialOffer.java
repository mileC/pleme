package opp.pleme.HandMadeShop.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "special_offer")
public class SpecialOffer {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @ManyToOne
    @JoinColumn(name = "users_id")
    private User user;

    @NotNull
    @Column(name = "specification", length = 120)
    private String specification;

    @Column(name = "media_link")
    private String mediaLink;

    @Column(name = "price")
    private float price;


    public SpecialOffer() {
    }

    @Column(name = "approved")
    private boolean approved;

    public SpecialOffer(User user, String specification, String mediaLink, float price, boolean approved) {
        this.user = user;
        this.specification = specification;
        this.mediaLink = mediaLink;
        this.price = price;
        this.approved = approved;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdUser() {
        return user.getId();
    }

    public String getUsername() {
        return user.getUsername();
    }

    public String getSpecification() {
        return specification;
    }

    public void setSpecification(String specification) {
        this.specification = specification;
    }

    public String getMediaLink() {
        return mediaLink;
    }

    public void setMediaLink(String mediaLink) {
        this.mediaLink = mediaLink;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SpecialOffer that = (SpecialOffer) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "SpecialOffer{" +
                "id=" + id +
                ", idUser=" + user.getUsername() +
                ", specification='" + specification + '\'' +
                ", mediaLink='" + mediaLink + '\'' +
                ", price=" + price +
                ", approved=" + approved +
                '}';
    }
}

