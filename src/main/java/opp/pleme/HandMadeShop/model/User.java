package opp.pleme.HandMadeShop.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Entity
@Table(name = "users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    @Column(name = "level_of_authority")
    private int levelOfAuthority;

    @NotNull
    @Column(name = "username")
    private String username;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @Column(name = "address")
    private String address;

    @Column(name = "contact")
    private String contact;

    @Column(name = "password")
    private String password;

    @Column(name = "card_number")
    private String cardNumber;

    @Column(name = "blocked")
    private boolean blocked = false;

    @Column(name = "wallet")
    private float wallet;

    public User(){
        this.username = "anonymous";
        this.wallet = addMoney();
        this.levelOfAuthority = 3;
    }

    public User(@NotNull String username, String name, String surname) {
        this.levelOfAuthority = 3;
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.wallet = addMoney();
    }

    public User(String username){
        this.username = username;
        this.wallet = addMoney();
        this.levelOfAuthority = 3;
    }

    public User(@NotNull String username, String name, String surname, String address, String contact, String password, String cardNumber, boolean blocked, @NotNull int levelOfAuthority) {
        this.levelOfAuthority = levelOfAuthority;
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.address = address;
        this.contact = contact;
        this.password = password;
        this.cardNumber = cardNumber;
        this.blocked = blocked;
        this.wallet = addMoney();
    }

    public float getWallet() {
        return wallet;
    }

    public void setWallet(float wallet) {
        this.wallet = wallet;
    }

    private float addMoney() {
        return (int) (Math.random() * 10000 + 1000);
    }

    public Long getId() {
        return id;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public void setId(Long id) {
        this.id = id;
    }

    public int getLevelOfAuthority() {
        return levelOfAuthority;
    }

    public void setLevelOfAuthority(int levelOfAuthority) {
        this.levelOfAuthority = levelOfAuthority;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }


    public boolean isBlocked() {
        return blocked;
    }

    public void setBlocked(boolean blocked) {
        this.blocked = blocked;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", levelOfAuthority=" + levelOfAuthority +
                ", username='" + username + '\'' +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", address='" + address + '\'' +
                ", contact='" + contact + '\'' +
                ", password='" + password + '\'' +
                ", cardNumber='" + cardNumber + '\'' +
                ", blocked=" + blocked +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User that = (User) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
