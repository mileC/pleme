package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.SpecialOffer;

public interface SpecialOfferService extends CrudService<SpecialOffer, Long> {
}
