package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.Item;
import opp.pleme.HandMadeShop.service.ItemService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class ItemMap extends AbstractMapService<Item, Long> implements ItemService {
    @Override
    public Set<Item> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Item object) {
        super.delete(object);
    }

    @Override
    public Item save(Item object) {
        return super.save(object.getId(),object);
    }

    @Override
    public Item findById(Long id) {
        return super.findById(id);
    }
}
