package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.SpecialOffer;
import opp.pleme.HandMadeShop.model.StylizedItem;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class ShoppingCartMap extends AbstractMapService<ShoppingCart, Long> implements ShoppingCartService {
    @Override
    public Set<ShoppingCart> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(ShoppingCart object) {
        super.delete(object);
    }

    @Override
    public ShoppingCart save(ShoppingCart object) {
        return super.save(object.getId(),object);
    }

    @Override
    public ShoppingCart findById(Long id) {
        return super.findById(id);
    }

    public float izracunajUkupnuCijenu(StylizedItem ... items){
        float cijena=0;
        if(items.length==0){
            return 0;
        }
        for (StylizedItem item : items){
            cijena+=item.getPriceOfStylizedItem();
        }
        return cijena;
    }

    @Override
    public void spremiUKosaricu(String username, StylizedItem stylizedItem, float totalCost) {

    }

}
