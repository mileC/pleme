package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.StylizedItem;
import opp.pleme.HandMadeShop.service.StylizedItemService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class StylizedItemMap extends AbstractMapService<StylizedItem, Long> implements StylizedItemService {
    @Override
    public Set<StylizedItem> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(StylizedItem object) {
        super.delete(object);
    }

    @Override
    public StylizedItem save(StylizedItem object) {
        return super.save(object.getId(),object);
    }

    @Override
    public StylizedItem findById(Long id) {
        return super.findById(id);
    }


}
