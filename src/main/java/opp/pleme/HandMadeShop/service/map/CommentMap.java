package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.Comment;
import opp.pleme.HandMadeShop.service.CommentService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class CommentMap extends AbstractMapService<Comment, Long> implements CommentService {
@Override
public Set<Comment> findAll() {
        return super.findAll();
        }

@Override
public void deleteById(Long id) {
        super.deleteById(id);
        }

@Override
public void delete(Comment object) {
        super.delete(object);
        }

@Override
public Comment save(Comment object) {
        return super.save(object.getId(),object);
        }

@Override
public Comment findById(Long id) {
        return super.findById(id);
        }
}

