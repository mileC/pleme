package opp.pleme.HandMadeShop.service.map;

import java.util.*;

public abstract class AbstractMapService<T, ID> {

    protected Map<ID, T> mapa= new HashMap<>();

    T save (ID id, T object){
        mapa.put(id, object);
        return object;
    }

    Set<T> findAll(){
        return  new HashSet<>(mapa.values());
    }

    T findById(ID id){
        return mapa.get(id);
    }

    void deleteById(ID id){
        mapa.remove(id);
    }

    void delete(T object){
        mapa.entrySet().removeIf(entry-> entry.getValue().equals(object));
    }

}
