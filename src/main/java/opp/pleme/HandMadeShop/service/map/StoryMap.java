package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.Story;
import opp.pleme.HandMadeShop.service.StoryService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class StoryMap extends AbstractMapService<Story, Long> implements StoryService {
    @Override
    public Set<Story> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Story object) {
        super.delete(object);
    }

    @Override
    public Story save(Story object) {
        return super.save(object.getId(),object);
    }

    @Override
    public Story findById(Long id) {
        return super.findById(id);
    }
}