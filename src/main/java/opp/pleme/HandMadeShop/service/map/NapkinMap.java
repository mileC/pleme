package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.Napkin;
import opp.pleme.HandMadeShop.service.NapkinService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class NapkinMap extends AbstractMapService<Napkin, Long> implements NapkinService {
    @Override
    public Set<Napkin> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Napkin object) {
        super.delete(object);
    }

    @Override
    public Napkin save(Napkin object) {
        return super.save(object.getId(),object);
    }

    @Override
    public Napkin findById(Long id) {
        return super.findById(id);
    }
}

