package opp.pleme.HandMadeShop.service.map;


import opp.pleme.HandMadeShop.forms.*;
import opp.pleme.HandMadeShop.model.*;
import opp.pleme.HandMadeShop.service.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.Set;

@Service
@Profile({"default", "map"})

public class UserMap extends AbstractMapService<User, Long> implements UserService {

    private final StoryService storyService;
    private final ItemService itemService;
    private final StyleService styleService;
    private final NapkinService napkinService;
    private final CommentService commentService;
    private final UserService userService;


    public UserMap(StoryService storyService, ItemService itemService, StyleService styleService, NapkinService napkinService, CommentService commentService, @Lazy UserService userService) {

        this.storyService = storyService;
        this.itemService = itemService;
        this.styleService = styleService;
        this.napkinService = napkinService;
        this.commentService = commentService;
        this.userService = userService;
    }

    @Override
    public Set<User> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(User object) {
        super.delete(object);
    }

    @Override
    public User save(User object) {
        return super.save(object.getId(), object);
    }

    @Override
    public User findById(Long id) {
        return super.findById(id);
    }

    public void predloziPricu(CreateStoryForm createStoryForm) {

    }

    @Override
    public Optional<User> findById(long userId) {
        return Optional.empty();
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return Optional.empty();
    }

    @Override
    public Optional<User> findByContact(String contact) {
        return Optional.empty();
    }

    @Override
    public Optional<User> findByUsernameAndPassword(String username, String password) {
        return Optional.empty();
    }

    @Override
    public int countByUsername(String nickname) {
        return 0;
    }

    @Override
    public int countByContact(String email) {
        return 0;
    }

    @Override
    public void odbijCijenu(AcceptPriceForm apf) {

    }

    @Override
    public void prihvatiCijenu(AcceptPriceForm apf) {

    }

    @Override
    public void prihvatiPricu(AcceptStoryForm asf) {

    }

    @Override
    public int platiKosaricuAnon(PaymentAnonForm paf) {
        return 0;
    }

    public void dodajPredmet(ItemsListForm itf) {
       return;
    }

    @Override
    public void stvoriPricu(CreateStoryForm csf) {

    }

    @Override
    public void naruciUkrasavanje(CreateSpecialOfferForm sof) {

    }

    @Override
    public int platiKosaricu(PaymentForm pf) {
        return 0;
    }

    public void dodajStil(StyleForm k) {
        return;
    }

    @Override
    public void dodajSalvetu(NapkinForm nf) {
        return;
    }

    public void omoguciPristup(User korisnik) {
            korisnik.setBlocked(false);
    }

    public void zabraniPristup(User korisnik) {
            korisnik.setBlocked(true);
    }

    @Override
    public User registrirajSe(User user) {
        return null;
    }

    @Override
    public User dodajAnonimnog(User user) {
        return null;
    }

    public void komentirajPricu(User user, String text, Story story) {
        commentService.save(new Comment(story, user, text));
    }

    @Override
    public void komentirajPricuFormom(CommentForm commentForm) {

    }

    @Override
    public void ponudiCijenu(OfferPriceForm opf) {

    }

    @Override
    public int urediProfil(String address, String cardNumber, String username) {
        return 0;
    }

}
