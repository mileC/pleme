package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.SpecialOffer;
import opp.pleme.HandMadeShop.service.SpecialOfferService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class SpecialOfferMap extends AbstractMapService<SpecialOffer, Long> implements SpecialOfferService {
    @Override
    public Set<SpecialOffer> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(SpecialOffer object) {
        super.delete(object);
    }

    @Override
    public SpecialOffer save(SpecialOffer object) {
        return super.save(object.getId(),object);
    }

    @Override
    public SpecialOffer findById(Long id) {
        return super.findById(id);
    }
}
