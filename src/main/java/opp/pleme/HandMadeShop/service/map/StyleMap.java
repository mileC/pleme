package opp.pleme.HandMadeShop.service.map;

import opp.pleme.HandMadeShop.model.Style;
import opp.pleme.HandMadeShop.service.StyleService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.Set;
@Service
@Profile({"default", "map"})
public class StyleMap  extends AbstractMapService<Style, Long> implements StyleService {
    @Override
    public Set<Style> findAll() {
        return super.findAll();
    }

    @Override
    public void deleteById(Long id) {
        super.deleteById(id);
    }

    @Override
    public void delete(Style object) {
        super.delete(object);
    }

    @Override
    public Style save(Style object) {
        return super.save(object.getId(),object);
    }

    @Override
    public Style findById(Long id) {
        return super.findById(id);
    }
}
