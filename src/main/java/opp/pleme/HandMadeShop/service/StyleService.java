package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.Style;

public interface StyleService extends CrudService<Style, Long> {
}
