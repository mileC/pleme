package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.Comment;
import opp.pleme.HandMadeShop.repositories.CommentRepository;
import opp.pleme.HandMadeShop.service.CommentService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class CommentSDJpaService implements CommentService {

    private final CommentRepository commentRepository;

    public CommentSDJpaService(CommentRepository commentRepository){
        this.commentRepository = commentRepository;
    }


    @Override
    public Set<Comment> findAll() {
        Set<Comment> comments = new HashSet<>();
        commentRepository.findAll().forEach(comments::add);
        return comments;
    }

    @Override
    public Comment findById(Long aLong) {
        return commentRepository.findById(aLong).orElse(null);
    }

    @Override
    public Comment save(Comment object) {
        return commentRepository.save(object);
    }

    @Override
    public void delete(Comment object) {
        commentRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        commentRepository.deleteById(aLong);

    }
}
