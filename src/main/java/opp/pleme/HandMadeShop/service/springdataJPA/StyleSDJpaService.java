package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.Style;
import opp.pleme.HandMadeShop.repositories.StyleRepository;
import opp.pleme.HandMadeShop.service.StyleService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class StyleSDJpaService implements StyleService {

    StyleRepository styleRepository;

    public StyleSDJpaService (StyleRepository styleRepository){
        this.styleRepository = styleRepository;
    }

    @Override
    public Set<Style> findAll() {
        Set<Style> styles = new HashSet<>();
        styleRepository.findAll().forEach(styles::add);
        return styles;
    }

    @Override
    public Style findById(Long aLong) {
        return styleRepository.findById(aLong).orElse(null);
    }

    @Override
    public Style save(Style object) {
        return styleRepository.save(object);
    }

    @Override
    public void delete(Style object) {
        styleRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        styleRepository.deleteById(aLong);
    }
}
