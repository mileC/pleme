package opp.pleme.HandMadeShop.service.springdataJPA;


import opp.pleme.HandMadeShop.forms.*;
import opp.pleme.HandMadeShop.model.*;
import opp.pleme.HandMadeShop.repositories.*;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class UserSDJpaService implements UserService {

    private final UserRepository userRepository;
    private final CommentRepository commentRepository;
    private final StoryRepository storyRepository;
    private final StyleRepository styleRepository;
    private final ItemRepository itemRepository;
    private final NapkinRepository napkinRepository;
    private final SpecialOfferRepository specialOfferRepository;
    private final RequestedSpecialOfferRepository requestedSpecialOfferRepository;
    private final ShoppingCartRepository shoppingCartRepository;
    private final TransactionsRepository transactionsRepository;
    private final FormRepository formRepository;
    private final StylizedItemRepository stylizedItemRepository;

    public UserSDJpaService(UserRepository userRepository, CommentRepository commentRepository,
                            StoryRepository storyRepository, StyleRepository styleRepository,
                            ItemRepository itemRepository, NapkinRepository napkinRepository,
                            SpecialOfferRepository specialOfferRepository, RequestedSpecialOfferRepository requestedSpecialOfferRepository, ShoppingCartRepository shoppingCartRepository, TransactionsRepository transactionsRepository, FormRepository formRepository, StylizedItemRepository stylizedItemRepository) {
        this.userRepository = userRepository;
        this.commentRepository = commentRepository;
        this.storyRepository = storyRepository;
        this.styleRepository = styleRepository;
        this.itemRepository = itemRepository;
        this.napkinRepository = napkinRepository;
        this.specialOfferRepository = specialOfferRepository;
        this.requestedSpecialOfferRepository = requestedSpecialOfferRepository;
        this.shoppingCartRepository = shoppingCartRepository;
        this.transactionsRepository = transactionsRepository;
        this.formRepository = formRepository;
        this.stylizedItemRepository = stylizedItemRepository;
    }

    @Override
    public User registrirajSe(User user) {
        return this.save(user);
    }

    @Override
    public User dodajAnonimnog(User user) {
        user.setBlocked(false);
        user.setLevelOfAuthority(3);
        int random = (int) (Math.random() * 50 + 1);
        String username = "anonymus" + String.valueOf(random);
        user.setUsername(username);
        user.setName("NA");
        user.setSurname("NA");
        user.setWallet((int) (Math.random() * 10000 + 1000));
        return userRepository.save(user);
    }

    @Override
    public void komentirajPricu(User korisnik, String text, Story prica) {
        commentRepository.save(new Comment(prica, korisnik, text));
    }

    //story, user, text
    @Override
    public void komentirajPricuFormom(CommentForm commentForm) {
        if (!commentForm.getUserId().equals("0")) {
            Comment comment = new Comment(storyRepository.findById(Long.valueOf(commentForm.getStoryId())).orElse(null),
                    userRepository.findById(Long.valueOf(commentForm.getUserId())).orElse(null), commentForm.getText());
            commentRepository.save(comment);
        } else {
            Comment comment = new Comment(storyRepository.findById(Long.valueOf(commentForm.getStoryId())).orElse(null),
                    userRepository.findByUsername("anonymous").orElse(null), commentForm.getAnonymous());
            commentRepository.save(comment);
        }
    }

    @Override
    public void ponudiCijenu(OfferPriceForm opf) {
        SpecialOffer so = requestedSpecialOfferRepository.findById(Long.valueOf(opf.getSpecialOfferId())).orElse(null);
        so.setPrice(Float.valueOf(opf.getPrice()));
        specialOfferRepository.save(so);
    }

    @Override
    public int urediProfil(String address, String cardNumber, String username) {
        userRepository.updateUserSetAddressForIdNative(address, username);
        return userRepository.updateUserSetCardnumber(cardNumber, username);
    }

    @Override
    public void zabraniPristup(User korisnik) {
        korisnik.setBlocked(true);
        userRepository.save(korisnik);
    }

    @Override
    public void omoguciPristup(User korisnik) {
        korisnik.setBlocked(false);
        userRepository.save(korisnik);
    }

    @Override
    public void dodajSalvetu(NapkinForm nf) {
        Napkin napkin = new Napkin(nf.getOpis());
        napkinRepository.save(napkin);
    }

    @Override
    public void dodajStil(StyleForm styleForm) {
        Style style = new Style(styleForm.getName(), styleForm.getDescription(), styleForm.getCoefficient());
        styleRepository.save(style);
    }

    @Override
    public void dodajPredmet(ItemsListForm itf) {
        Item item = new Item(itf.getName(), itf.getLink(), itf.getDescription(), itf.getItemPrice());
        itemRepository.save(item);
    }

    @Override
    public void predloziPricu(CreateStoryForm csf) {
        Story story = new Story(csf.getTitle(), csf.getText(), csf.getVideo(), csf.getPicture(), false);
        storyRepository.save(story);
    }

    @Override
    public void stvoriPricu(CreateStoryForm csf) {
        Story story = new Story(csf.getTitle(), csf.getText(), csf.getVideo(), csf.getPicture(), true);
        storyRepository.save(story);
    }

    //    public SpecialOffer(User user, String specification, String mediaLink, float price, boolean approved) {
    @Override
    public void naruciUkrasavanje(CreateSpecialOfferForm sof) {
        SpecialOffer specialOffer = new SpecialOffer(userRepository.findById(Long.valueOf(sof.getUserId())).orElse(null),
                sof.getSpecification(), sof.getMediaLink(), 0, false);
        requestedSpecialOfferRepository.save(specialOffer);
        System.out.println(requestedSpecialOfferRepository.findAll());
    }

    @Override
    public int platiKosaricu(PaymentForm paymentForm) {
        User user = userRepository.findByUsername(paymentForm.getUsername()).orElse(
                userRepository.findByUsername("anonymous").get());

        Form form = new Form(paymentForm.getName(), paymentForm.getSurname(), paymentForm.getAddress(),
                Integer.valueOf(paymentForm.getCardNumber()), paymentForm.getContact());
        formRepository.save(form);

        System.out.println(form.getId());
        System.out.println(user);

        ShoppingCart shoppingCart = shoppingCartRepository.findByUsername(user.getUsername()).orElse(null);
        shoppingCart.setForm(form);

        if (shoppingCart.getTotalCost() <= user.getWallet()) {
            float newTakujin = (float) (user.getWallet() - shoppingCart.getTotalCost());
            user.setWallet(newTakujin);

            System.out.println(user.getWallet());
            //zas sejvas novog usera tu, treba mu samo updejtat walet!! sad je ok
            userRepository.updateUserSetWallet(newTakujin, user.getUsername());

            shoppingCart.setPaid(true);
            Transaction transaction=new Transaction(user, shoppingCart);
            transactionsRepository.save(transaction);

            System.out.println(transactionsRepository.count());
            stylizedItemRepository.deleteAllByShoppingCart(shoppingCart);
            shoppingCartRepository.delete(shoppingCart);

            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public int platiKosaricuAnon(PaymentAnonForm paf) {
        User user= userRepository.findByUsername(paf.getUsername()).get();

        user.setUsername(paf.getUsername());
        user.setName(paf.getName());
        user.setSurname(paf.getSurname());
        user.setAddress(paf.getAddress());
        user.setContact(paf.getContact());
        user.setCardNumber(paf.getCardNumber());

        Form form = new Form(paf.getName(), paf.getSurname(), paf.getAddress(),
                Integer.valueOf(paf.getCardNumber()), paf.getContact());
        formRepository.save(form);

        ShoppingCart shoppingCart = shoppingCartRepository.findByUsername(user.getUsername()).orElse(null);
        shoppingCart.setForm(form);

        if (shoppingCart.getTotalCost() <= user.getWallet()) {
            float newTakujin = (float) (user.getWallet() - shoppingCart.getTotalCost());
            user.setWallet(newTakujin);

            System.out.println(user.getWallet());
            //zas sejvas novog usera tu, treba mu samo updejtat walet!! sad je ok
            userRepository.updateUserSetWallet(newTakujin, user.getUsername());

            shoppingCart.setPaid(true);
            Transaction transaction=new Transaction(user, shoppingCart);
            transactionsRepository.save(transaction);

            stylizedItemRepository.deleteAllByShoppingCart(shoppingCart);
            shoppingCartRepository.delete(shoppingCart);

            return 0;
        } else {
            return 1;
        }
    }

    @Override
    public Optional<User> findById(long userId) {
        return userRepository.findById(userId);
    }

    @Override
    public Optional<User> findByUsername(String username) {
        return userRepository.findByUsername(username);
    }


    @Override
    public Optional<User> findByContact(String contact) {
        return userRepository.findByContact(contact);
    }

    @Override
    public Optional<User> findByUsernameAndPassword(String username, String password) {
        return userRepository.findByUsernameAndPassword(username, password);
    }

    @Override
    public int countByUsername(String username) {
        return userRepository.countByUsername(username);
    }

    @Override
    public int countByContact(String email) {
        return userRepository.countByContact(email);
    }

    @Override
    public void odbijCijenu(AcceptPriceForm apf) {
        SpecialOffer so = requestedSpecialOfferRepository.findById(Long.valueOf(apf.getSpecialOfferId())).orElse(null);
        specialOfferRepository.delete(so);
    }

    @Override
    public void prihvatiCijenu(AcceptPriceForm apf) {
        SpecialOffer so = requestedSpecialOfferRepository.findById(Long.valueOf(apf.getSpecialOfferId())).orElse(null);
        so.setApproved(true);
        specialOfferRepository.save(so);
    }

    @Override
    public void prihvatiPricu(AcceptStoryForm asf) {
        Story story = storyRepository.findById(Long.valueOf(asf.getSuggestedStoryId())).orElse(null);
        story.setApproved(true);
        storyRepository.save(story);
    }



    private boolean odobriPricu(String text) {
        if (storyRepository.save(new Story(text)) != null)
            return true;
        else
            return false;
    }

    @Override
    public Set<User> findAll() {
        Set<User> users = new HashSet<>();
        userRepository.findAll().forEach(users::add);
        return users;
    }

    @Override
    public User findById(Long aLong) {
        return userRepository.findById(aLong).orElse(null);
    }

    @Override
    public User save(User object) {
        return userRepository.save(object);
    }

    @Override
    public void delete(User object) {
        userRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        userRepository.deleteById(aLong);
    }
}
