package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.Form;
import opp.pleme.HandMadeShop.repositories.FormRepository;
import opp.pleme.HandMadeShop.service.FormService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class FormaSDJpaService implements FormService {

    private final FormRepository formRepository;

    public FormaSDJpaService(FormRepository formRepository){
        this.formRepository = formRepository;
    }

    @Override
    public Set<Form> findAll() {
        Set<Form> forms = new HashSet<>();
        formRepository.findAll().forEach(forms::add);
        return forms;
    }

    @Override
    public Form findById(Long aLong) {
        return formRepository.findById(aLong).orElse(null);
    }

    @Override
    public Form save(Form object) {
        return formRepository.save(object);
    }

    @Override
    public void delete(Form object) {
        formRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        formRepository.deleteById(aLong);
    }
}
