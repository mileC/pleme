package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.SpecialOffer;
import opp.pleme.HandMadeShop.model.StylizedItem;
import opp.pleme.HandMadeShop.repositories.ShoppingCartRepository;
import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class ShoppingCartSDJpaService implements ShoppingCartService {

    ShoppingCartRepository shoppingCartRepository;
    StylizedItemSDJpaService stylizedItemSDJpaService;

    public ShoppingCartSDJpaService(ShoppingCartRepository shoppingCartRepository, StylizedItemSDJpaService stylizedItemSDJpaService){
        this.shoppingCartRepository = shoppingCartRepository;
        this.stylizedItemSDJpaService=stylizedItemSDJpaService;
    }

    @Override
    public Set<ShoppingCart> findAll() {
        Set<ShoppingCart> shoppingCarts = new HashSet<>();
        shoppingCartRepository.findAll().forEach(shoppingCarts::add);
        return shoppingCarts;
    }

    @Override
    public ShoppingCart findById(Long aLong) {
        return shoppingCartRepository.findById(aLong).orElse(null);
    }

    @Override
    public ShoppingCart save(ShoppingCart object) {
        return shoppingCartRepository.save(object);
    }

    @Override
    public void delete(ShoppingCart object) {
        shoppingCartRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        shoppingCartRepository.deleteById(aLong);

    }

    @Override
    public void spremiUKosaricu(String username, StylizedItem stylizedItem, float totalCost) {

        Optional<ShoppingCart> shoppingCart= shoppingCartRepository.findByUsername(username);

        if(!shoppingCart.isPresent()){
            ShoppingCart newShoppingCart=new ShoppingCart();
            newShoppingCart.setUsername(username);
            stylizedItem.setShoppingCart(newShoppingCart);
            newShoppingCart.addItem(stylizedItem, totalCost);
            shoppingCartRepository.save(newShoppingCart);

            //stylizedItemSDJpaService.save(stylizedItem);
        } else {
            stylizedItem.setShoppingCart(shoppingCart.get());
            shoppingCart.get().addItem(stylizedItem, totalCost);
            shoppingCartRepository.save(shoppingCart.get());

           // stylizedItemSDJpaService.save(stylizedItem);
        }
    }
}
