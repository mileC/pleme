package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.SpecialOffer;
import opp.pleme.HandMadeShop.repositories.SpecialOfferRepository;
import opp.pleme.HandMadeShop.service.SpecialOfferService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class SpecialOfferSDJpaService implements SpecialOfferService {

    SpecialOfferRepository specialOfferRepository;

    public SpecialOfferSDJpaService(SpecialOfferRepository specialOfferRepository){
        this.specialOfferRepository = specialOfferRepository;
    }

    @Override
    public Set<SpecialOffer> findAll() {
        Set<SpecialOffer> specialOffers = new HashSet<>();
        specialOfferRepository.findAll().forEach(specialOffers::add);
        return specialOffers;
    }

    @Override
    public SpecialOffer findById(Long aLong) {
        return specialOfferRepository.findById(aLong).orElse(null);
    }

    @Override
    public SpecialOffer save(SpecialOffer object) {
        return specialOfferRepository.save(object);
    }

    @Override
    public void delete(SpecialOffer object) {
        specialOfferRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        specialOfferRepository.deleteById(aLong);
    }
}
