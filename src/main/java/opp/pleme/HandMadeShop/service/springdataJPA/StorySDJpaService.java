package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.Comment;
import opp.pleme.HandMadeShop.model.Story;
import opp.pleme.HandMadeShop.repositories.CommentRepository;
import opp.pleme.HandMadeShop.repositories.StoryRepository;
import opp.pleme.HandMadeShop.service.StoryService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class StorySDJpaService implements StoryService {

    StoryRepository storyRepository;
    CommentRepository commentRepository;

    public StorySDJpaService (StoryRepository storyRepository, CommentRepository commentRepository){
        this.storyRepository = storyRepository;
        this.commentRepository = commentRepository;
    }

    @Override
    public Set<Story> findAll() {
        Set<Story> stories = new HashSet<>();
        storyRepository.findAll().forEach(stories::add);
        return stories;
    }

    @Override
    public Story findById(Long aLong) {
        return storyRepository.findById(aLong).orElse(null);
    }

    @Override
    public Story save(Story object) {
        return storyRepository.save(object);
    }

    @Override
    public void delete(Story object) {
        storyRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        storyRepository.findById(aLong);
    }
}
