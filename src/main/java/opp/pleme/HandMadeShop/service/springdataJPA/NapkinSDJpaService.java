package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.Napkin;
import opp.pleme.HandMadeShop.repositories.NapkinRepository;
import opp.pleme.HandMadeShop.service.NapkinService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class NapkinSDJpaService implements NapkinService {

    NapkinRepository napkinRepository;

    public NapkinSDJpaService(NapkinRepository napkinRepository){
        this.napkinRepository = napkinRepository;
    }


    @Override
    public Set<Napkin> findAll() {
        Set<Napkin> napkins = new HashSet<>();
        napkinRepository.findAll().forEach(napkins::add);
        return napkins;
    }

    @Override
    public Napkin findById(Long aLong) {
        return napkinRepository.findById(aLong).orElse(null);
    }

    @Override
    public Napkin save(Napkin object) {
        return napkinRepository.save(object);
    }

    @Override
    public void delete(Napkin object) {
        napkinRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        napkinRepository.deleteById(aLong);
    }
}
