package opp.pleme.HandMadeShop.service.springdataJPA;

import opp.pleme.HandMadeShop.model.StylizedItem;
import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import opp.pleme.HandMadeShop.service.StylizedItemService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@Profile("springDataJPA")
public class StylizedItemSDJpaService implements StylizedItemService {


    StylizedItemRepository stylizedItemRepository;

    public StylizedItemSDJpaService(StylizedItemRepository stylizedItemRepository){
        this.stylizedItemRepository = stylizedItemRepository;
    }

    @Override
    public Set<StylizedItem> findAll() {
        Set<StylizedItem> stylizedItems = new HashSet<>();
        stylizedItemRepository.findAll().forEach(stylizedItems::add);
        return stylizedItems;
    }

    @Override
    public StylizedItem findById(Long aLong) {
        return stylizedItemRepository.findById(aLong).orElse(null);
    }

    @Override
    public StylizedItem save(StylizedItem object) {
        return stylizedItemRepository.save(object);
    }

    @Override
    public void delete(StylizedItem object) {
        stylizedItemRepository.delete(object);
    }

    @Override
    public void deleteById(Long aLong) {
        stylizedItemRepository.deleteById(aLong);
    }
}
