package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.Item;

public interface ItemService extends CrudService<Item, Long> {
}
