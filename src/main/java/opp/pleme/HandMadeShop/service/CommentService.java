package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.Comment;

public interface CommentService extends CrudService<Comment, Long> {
}
