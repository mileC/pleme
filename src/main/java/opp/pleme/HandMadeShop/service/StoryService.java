package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.Story;

public interface StoryService extends CrudService<Story, Long> {
}
