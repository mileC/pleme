package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.SpecialOffer;
import opp.pleme.HandMadeShop.model.StylizedItem;

import java.util.Optional;

public interface ShoppingCartService extends CrudService<ShoppingCart, Long> {
    void spremiUKosaricu(String username, StylizedItem stylizedItem, float totalCost);
}
