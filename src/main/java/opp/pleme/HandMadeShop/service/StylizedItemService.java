package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.StylizedItem;

public interface StylizedItemService extends CrudService<StylizedItem, Long> {
}
