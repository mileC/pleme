package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.Form;

public interface FormService extends CrudService<Form, Long> {
}
