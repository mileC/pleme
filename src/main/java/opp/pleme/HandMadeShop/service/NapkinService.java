package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.model.Napkin;

public interface NapkinService extends CrudService<Napkin, Long> {
}
