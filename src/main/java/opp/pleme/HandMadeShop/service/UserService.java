package opp.pleme.HandMadeShop.service;

import opp.pleme.HandMadeShop.forms.*;
import opp.pleme.HandMadeShop.model.*;

import java.util.Optional;

public interface UserService extends CrudService<User, Long> {

    User registrirajSe(User user);

    User dodajAnonimnog(User user);

    void komentirajPricu(User korisnik, String text, Story prica);

    void komentirajPricuFormom(CommentForm cf);

    void ponudiCijenu(OfferPriceForm opf);

    int urediProfil(String address, String cardNumber, String username);

    void zabraniPristup(User korisnik);

    void omoguciPristup(User korisnik);

    void dodajSalvetu(NapkinForm nf);

    void dodajStil(StyleForm styleForm);

    void dodajPredmet(ItemsListForm itf);

    void predloziPricu(CreateStoryForm csf);

    void stvoriPricu(CreateStoryForm csf);

    void naruciUkrasavanje(CreateSpecialOfferForm sof);

    int platiKosaricu(PaymentForm pf);

    Optional<User> findById(long userId);

    Optional<User> findByUsername(String username);

    Optional<User> findByContact(String contact);

    Optional<User> findByUsernameAndPassword(String username, String password);

    int countByUsername(String nickname);

    int countByContact(String email);

    void odbijCijenu(AcceptPriceForm apf);

    void prihvatiCijenu(AcceptPriceForm apf);

    void prihvatiPricu(AcceptStoryForm asf);

    int platiKosaricuAnon(PaymentAnonForm paf);
}
