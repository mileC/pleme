package opp.pleme.HandMadeShop.exceptions;

public class RequestDeniedException extends Exception {
    public String message;

    public RequestDeniedException(){}

    public RequestDeniedException(String message){
        this.message=message;
    }
}
