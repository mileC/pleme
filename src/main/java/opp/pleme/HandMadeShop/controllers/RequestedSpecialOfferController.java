package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.OfferPriceForm;
import opp.pleme.HandMadeShop.model.SpecialOffer;
import opp.pleme.HandMadeShop.repositories.RequestedSpecialOfferRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/requestedSpecialOffers")
public class RequestedSpecialOfferController {

    @Autowired
    private RequestedSpecialOfferRepository requestedSpecialOfferRepository;
    @Autowired
    private UserService userService;

    boolean allApproved = true;

    @GetMapping("")
    public String getForm(Model model, HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null
                || ((int) request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }
        for (SpecialOffer so : requestedSpecialOfferRepository.findAll())
            if (!so.isApproved()) {
                allApproved = false;
            } else {
                allApproved = true;
            }

        model.addAttribute("allApproved", allApproved);

        model.addAttribute("requestedSpecialOffers", requestedSpecialOfferRepository.findAll());
        return "requestedSpecialOffers";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String offerSpecialOfferPrice(@RequestParam("method") String method, HttpServletRequest request, Model
            model) {

        model.addAttribute("requestedSpecialOffers", requestedSpecialOfferRepository.findAll());

        OfferPriceForm opf = new OfferPriceForm();
        opf.popuniFormIzHttp(request);
        userService.ponudiCijenu(opf);

        return "requestedSpecialOffers";
    }
}
