package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.NapkinForm;
import opp.pleme.HandMadeShop.repositories.NapkinRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class NapkinController {
    @Autowired
    private NapkinRepository napkinRepository;

    @Autowired
    private UserService userService;

    public NapkinController(NapkinRepository napkinRepository, UserService userService) {
        this.napkinRepository = napkinRepository;
        this.userService = userService;
    }

    @GetMapping("/napkins")
    public String getNapkins(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("curUserId") == null
                || ((int) request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }

        model.addAttribute("napkins", napkinRepository.findAll());
        return "napkins";
    }

    @RequestMapping(value = "/napkins", method = RequestMethod.POST)
    public String dodajSalvetu(@RequestParam("method") String method, HttpServletRequest request, Model model) {


        NapkinForm nf = new NapkinForm();
        nf.popuniNapkinFormIzHttp(request);
        userService.dodajSalvetu(nf);
        return "redirect:/napkins";
    }
}

