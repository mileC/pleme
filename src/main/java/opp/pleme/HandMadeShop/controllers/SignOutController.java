package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.StylizedItem;
import opp.pleme.HandMadeShop.repositories.ShoppingCartRepository;
import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/signOut")
public class SignOutController {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private StylizedItemRepository stylizedItemRepository;

    @GetMapping("")
    public String getForm(HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null) {
            return "redirect:/";
        }
        return "signOut";
    }

    @RequestMapping
    public String signOut(@RequestParam("method") String method, HttpServletRequest request, Model model) {

        if (!method.equals("signout")) {
            return "redirect:/";
        } else {
            if (shoppingCartRepository.findByUsername((String) request.getSession().getAttribute("curUserUsername")).isPresent()) {
                if (!shoppingCartRepository.findByUsername((String) request.getSession().getAttribute("curUserUsername")).isPresent()) {
                    ShoppingCart shoppingCart = new ShoppingCart();
                    shoppingCart.setUsername((String) request.getSession().getAttribute("curUserUsername"));
                    shoppingCartRepository.save(shoppingCart);
                }
                request.getSession().invalidate();
                return "redirect:/";
            }
            request.getSession().invalidate();
            return "redirect:/";
        }
       // return "redirect:/";
    }
}