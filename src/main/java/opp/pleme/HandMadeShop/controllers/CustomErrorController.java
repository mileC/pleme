package opp.pleme.HandMadeShop.controllers;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpServletRequest;

@Controller
public class CustomErrorController implements ErrorController {

    @RequestMapping("/error")
    @ResponseBody
    public String handleError(HttpServletRequest request) {
        return "Ups! Došlo je do pogreške! Vratite se na prethodnu stranicu " +
                "i pazite što radite :)";
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}