package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.RegisterForm;
import opp.pleme.HandMadeShop.model.User;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserService userService;

    @ModelAttribute("registerform")
    public RegisterForm populateFeatures() {
        return new RegisterForm();
    }

    @GetMapping("")
    public String getForm(HttpServletRequest request){
        if (request.getSession().getAttribute("curUserId") == null) {
            return "register";
        }
        return "redirect:/";
    }

    @RequestMapping
    public String registerUser(@RequestParam("method") String method,final User user, final BindingResult bindingResult,
                               Model model, HttpServletRequest request) {
       if(!method.equals("save")) {
           return "redirect:/";
       }
        if (bindingResult.hasErrors()) {
            return "register";
        }

       RegisterForm rf = new RegisterForm();
       rf.popuniRegFormIzHttp(request);
       rf.validate();

       checkIfValidWithDB(rf);

       if(!rf.isValid()){
           model.addAttribute("registerform", rf);
           return "register";
       }

       User newUser=new User();
       rf.popuniURecord(newUser);
       newUser.setLevelOfAuthority(1);
       newUser.setBlocked(false);
       newUser.setWallet((int) (Math.random() * 10000 + 1000));
       

       userService.registrirajSe(newUser);

       model.addAttribute("registerform", new RegisterForm());
       return "redirect:/login";

   }

    private void checkIfValidWithDB(RegisterForm rf) {
        if(userService.countByUsername(rf.getUsername())>0){
            rf.setError("username", "Username se već koristi!");
        }
        if(userService.countByContact(rf.getContact())>0){
            rf.setError("contact", "Email se već koristi!");
        }
    }
}
