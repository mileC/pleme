package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.CreateStoryForm;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/createStory")
public class CreateStoryController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public String getForm(HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null
                || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }
        return "createStory";
    }

    @RequestMapping
    public String createStory(@RequestParam("method") String method, HttpServletRequest request, Model model) {
        if (!method.equals("save")) {
            return "redirect:/";
        }

        CreateStoryForm csf = new CreateStoryForm();
        csf.popuniRegFormIzHttp(request);
        csf.validate();
        userService.stvoriPricu(csf);
        return "redirect:/stories";
    }

}

