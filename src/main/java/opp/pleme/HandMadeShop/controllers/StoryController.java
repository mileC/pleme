package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.model.Comment;
import opp.pleme.HandMadeShop.model.Story;
import opp.pleme.HandMadeShop.repositories.StoryRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class StoryController {
    private StoryRepository storyRepository;

    @Autowired
    private UserService userService;

    public StoryController(StoryRepository storyRepository) {
        this.storyRepository = storyRepository;
    }

    @RequestMapping("/stories")
    public String getStories(Model model) {
        model.addAttribute("stories", storyRepository.findAll());

        Map<Story, List<Comment>> mapa = new HashMap<>();
        for (Story story : storyRepository.findAll()) {
            mapa.put(story, story.getComments());
            model.addAttribute("storyCommentMap", mapa);
        }
        return "stories";
    }

}

