package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.PaymentAnonForm;
import opp.pleme.HandMadeShop.forms.PaymentForm;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/fillForm")
public class FormController {

    @Autowired
    private UserService userService;

    @ModelAttribute("payanonform")
    public PaymentAnonForm populateFeatures() {
        return new PaymentAnonForm();
    }


    @GetMapping("")
    public String getForm(HttpServletRequest request, Model model) {
        /*if (request.getSession().getAttribute("curUserId") == null
                || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 1)) {
            return "redirect:/";
        }*/

        model.addAttribute("korisnik", (String)request.getSession().getAttribute("curUserUsername"));
        model.addAttribute("idAnon", (String)request.getSession().getId());
        return "fillForm";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String getForm(@RequestParam("method") String method, HttpServletRequest request, Model model) {
        if (!method.startsWith("buy")) {
            return "index";
        }

        if(request.getSession().getAttribute("curUserUsername")!=null) {
            PaymentForm pf = new PaymentForm();
            pf.popuniFormIzHttpa(request);
            if (userService.platiKosaricu(pf) == 0) {
                return "redirect:/success";
            } else {
                return "thinWallet";
            }
        } else {
            PaymentAnonForm paf= new PaymentAnonForm();
            paf.popuniFormIzHttpa(request);
            if (userService.platiKosaricuAnon(paf) == 0) {
                return "redirect:/success";
            } else {
                return "thinWallet";
            }
        }



    }
}

