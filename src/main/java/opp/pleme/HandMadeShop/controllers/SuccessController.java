package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.model.User;
import opp.pleme.HandMadeShop.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

@Controller
public class SuccessController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/success")
    public String getSuccess(Model model, HttpServletRequest request){
        float wallet=0f;
        if(request.getSession().getAttribute("curUserUsername")==null){
            User user=userRepository.findByUsername(request.getSession().getId()).get();
            wallet=userRepository.findByUsername(request.getSession().getId()).get().getWallet();
            userRepository.delete(user);
        } else {
            wallet = userRepository.findByUsername(String.valueOf(request.getSession().
                    getAttribute("curUserUsername"))).get().getWallet();
        }

        model.addAttribute("wallet", wallet);
        return "success";
    }

}

