package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.StylizedItem;
import opp.pleme.HandMadeShop.model.User;
import opp.pleme.HandMadeShop.repositories.ShoppingCartRepository;
import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ShoppingCartController {
    private ShoppingCartRepository shoppingCartRepository;
    private ShoppingCartService shoppingCartService;
    private UserService userService;
    private StylizedItemRepository stylizedItemRepository;

    public ShoppingCartController(ShoppingCartRepository shoppingCartRepository, ShoppingCartService shoppingCartService,
                                  UserService userService, StylizedItemRepository stylizedItemRepository) {
        this.shoppingCartRepository = shoppingCartRepository;
        this.shoppingCartService = shoppingCartService;
        this.userService = userService;
        this.stylizedItemRepository = stylizedItemRepository;
    }

    @GetMapping("/shoppingCart")
    public String getShoppingCart(HttpServletRequest request, Model model) {

        String username = (String) request.getSession().getAttribute("curUserUsername");
        if (username == null) {
            username = request.getSession().getId();
        }

        if (shoppingCartRepository.findByUsername(username).isPresent()) {
            model.addAttribute("shoppingCart", shoppingCartRepository.findByUsername(username).get().getItems());
            model.addAttribute("price", shoppingCartRepository.findByUsername(username).get().getTotalCost());
            model.addAttribute("items", shoppingCartRepository.findByUsername(username).get().getItems().isEmpty());
        }
        return "shoppingCart";
    }

    @RequestMapping("/shoppingCart")
    public String fillForm(@RequestParam("method") String method, Model model, HttpServletRequest request) {
        String username = (String) request.getSession().getAttribute("curUserUsername");
        if (username == null) {
            username = request.getSession().getId();
        }

        if (method.equals("proceed")) {
            if (request.getSession().getAttribute("curUserUsername") == null) {
                User user = new User((String) request.getSession().getId());
                userService.save(user);
            }
            if ( !shoppingCartRepository.findByUsername(username).isPresent()) {
                return "redirect:/shoppingCart";
            }
            return "redirect:/fillForm";
        } else if (method.equals("deleteItems")) {
            if ( !shoppingCartRepository.findByUsername(username).isPresent()) {
                return "redirect:/shoppingCart";
            }
            ShoppingCart sc = shoppingCartRepository.findByUsername(username).get();
            stylizedItemRepository.deleteAllByShoppingCart(sc);
            shoppingCartService.delete(sc);
            return "redirect:/shoppingCart";
        }
        return "redirect:/index";
    }
}



