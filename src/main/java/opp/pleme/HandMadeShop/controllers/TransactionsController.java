package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.repositories.TransactionsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/transactions")
public class TransactionsController {

    @Autowired
    private TransactionsRepository transactionsRepository;

    public TransactionsController(TransactionsRepository transactionsRepository) {
        this.transactionsRepository = transactionsRepository;
    }

    @GetMapping("")
    public String getTransactions(Model model) {
        System.out.println(transactionsRepository.findAll());
        model.addAttribute("transactions", transactionsRepository.findAll());
        return "transactions";
    }

}

