package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.BlockUserForm;
import opp.pleme.HandMadeShop.repositories.UserRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/regusers")
public class RegisteredUserController {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserService userService;

    @GetMapping("")
    public String getForm(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("curUserId") == null
        || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }

        model.addAttribute("registeredUsers", userRepository.findAll());
        return "regusers";
    }


    public RegisteredUserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String getRegisteredUsers(@RequestParam("method") String method, HttpServletRequest request, Model model) {

        BlockUserForm buf = new BlockUserForm();
        buf.popuniFormIzHttp(request);

        if (method.equals("deny")) {
            userService.zabraniPristup(userRepository.findById
                    (Long.valueOf(buf.getUserId())).orElse(null));
            return "redirect:/regusers";
        } else if (method.equals("grant")) {
            userService.omoguciPristup(userRepository.findById
                    (Long.valueOf(buf.getUserId())).orElse(null));
            return "redirect:/regusers";
        }
        model.addAttribute("registeredUsers", userRepository.findAll());
        return "regusers";
    }
}

