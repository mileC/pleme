package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.ItemsListForm;
import opp.pleme.HandMadeShop.repositories.ItemRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ItemsListController {
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private UserService userService;

    public ItemsListController(ItemRepository itemRepository, UserService userService) {
        this.itemRepository = itemRepository;
        this.userService = userService;
    }

    @GetMapping("/itemsList")
    public String getItemsList(Model model, HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null
                || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }
        model.addAttribute("itemsList", itemRepository.findAll());
        return "itemsList";
    }

    @RequestMapping(value = "/itemsList", method = RequestMethod.POST)
    public String addItem(@RequestParam("method") String method, HttpServletRequest request, Model model) {
        if (!method.equals("save")) {
            return "redirect:/itemsList";
        }

        ItemsListForm itf = new ItemsListForm();
        itf.popuniItemsListFormIzHttp(request);
        userService.dodajPredmet(itf);
        return "redirect:/itemsList";
    }
}
