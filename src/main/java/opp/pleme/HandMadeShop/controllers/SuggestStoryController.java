package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.CreateSpecialOfferForm;
import opp.pleme.HandMadeShop.forms.CreateStoryForm;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/suggestStory")
public class SuggestStoryController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public String getForm(HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null
                || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 1)) {
            return "redirect:/";
        }
        return "suggestStory";
    }

    @RequestMapping
    public String suggestStory(@RequestParam("method") String method, HttpServletRequest request, Model model) {

        CreateStoryForm csf = new CreateStoryForm();
        csf.popuniRegFormIzHttp(request);
        csf.validate();
        userService.predloziPricu(csf);
        return "redirect:/stories";
    }

}
