package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class StylizedItemController {
    private StylizedItemRepository stylizedItemRepository;

    public StylizedItemController(StylizedItemRepository stylizedItemRepository ) {
        this.stylizedItemRepository = stylizedItemRepository;
    }

    @RequestMapping("/StylizedItems")
    public String getStylizedItems (Model model){

        model.addAttribute("StylizedItems", stylizedItemRepository.findAll());

        return "StylizedItems";
    }
}

