package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.EditProfileForm;
import opp.pleme.HandMadeShop.model.User;
import opp.pleme.HandMadeShop.service.UserService;

import opp.pleme.HandMadeShop.util.Util;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import javax.servlet.http.HttpServletRequest;

@Controller
public class ProfileController {

    @Autowired
    private UserService userService;

    @ModelAttribute("editprofileform")
    public EditProfileForm populateFeatures() {
        return new EditProfileForm();
    }

    @GetMapping("/profile")
    public String getEdit(HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null) {
            return "redirect:/";
        }
        return "profile";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.POST)
    public String showProfile(@RequestParam("method") String method, HttpServletRequest request, Model model) {
        if (!method.equals("save")) {
            return "redirect:/index";
        }

        EditProfileForm epf = new EditProfileForm();
        epf.popuniRegFormIzHttp(request);
        epf.validate();

        if (!epf.isValid()) {
            model.addAttribute("editprofileform", epf);
            return "profile";
        }

        User op = userService.findByUsername(epf.getUsername()).get();

        epf.popuniURecord(op);
        Long id = (Long) request.getSession().getAttribute("curUserId");

        if (id.equals(op.getId())) {
            userService.urediProfil(epf.getAddress(), epf.getCard_number(), epf.getUsername());
            Util.setSessionAttributes(request, op);
        }

        userService.urediProfil(epf.getAddress(), epf.getCard_number(), epf.getUsername());

        model.addAttribute("editprofileform", new EditProfileForm());
        return "redirect:/index";

    }

}