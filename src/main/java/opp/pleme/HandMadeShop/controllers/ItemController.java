package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.model.*;
import opp.pleme.HandMadeShop.repositories.ItemRepository;
import opp.pleme.HandMadeShop.repositories.NapkinRepository;
import opp.pleme.HandMadeShop.repositories.ShoppingCartRepository;
import opp.pleme.HandMadeShop.repositories.StyleRepository;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import opp.pleme.HandMadeShop.service.UserService;
import opp.pleme.HandMadeShop.service.springdataJPA.StylizedItemSDJpaService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class ItemController {
    private ItemRepository itemRepository;
    private StyleRepository styleRepository;
    private NapkinRepository napkinRepository;
    private ShoppingCartRepository shoppingCartRepository;
    private ShoppingCartService shoppingCartService;
    private UserService userService;
    private StylizedItemSDJpaService stylizedItemSDJpaService;

    public ItemController(ItemRepository itemRepository, StyleRepository styleRepository, NapkinRepository napkinRepository
    ,ShoppingCartRepository shoppingCartRepository,ShoppingCartService shoppingCartService,UserService userService,
                          StylizedItemSDJpaService stylizedItemSDJpaService) {
        this.napkinRepository = napkinRepository;
        this.styleRepository = styleRepository;
        this.itemRepository = itemRepository;
        this.shoppingCartRepository=shoppingCartRepository;
        this.shoppingCartService=shoppingCartService;
        this.userService=userService;
        this.stylizedItemSDJpaService=stylizedItemSDJpaService;
    }

    @GetMapping("/items")
    public String getItems(Model model, HttpServletRequest request) {
       // request.getSession();
        model.addAttribute("napkins", napkinRepository.findAll());
        model.addAttribute("styles", styleRepository.findAll());
        model.addAttribute("items", itemRepository.findAll());
        return "items";
    }

    @RequestMapping(value = "/items", method = RequestMethod.POST)
    public String dodajUKosaricu(@RequestParam("method") String method, @RequestParam("napkinSelected") String napkinSelected,
                                 @RequestParam("styleSelected") String styleSelected,
                                 @RequestParam("itemSelected") String itemSelected,
                                 Model model, HttpServletRequest request){
        if(!method.equals("save")){
            return "redirect:/items";
        }

        Napkin napkin=napkinRepository.findByName(napkinSelected);
        Style style=styleRepository.findByName(styleSelected);
        Item item= itemRepository.findByName(itemSelected);

        float itemPrice=item.getItemPrice();
        float coefficient=style.getCoefficient();
        float totalCost=itemPrice*coefficient;

        StylizedItem stylizedItem=new StylizedItem(napkin, item,style);
        stylizedItem.setPriceOfStylizedItem(totalCost);
        stylizedItemSDJpaService.save(stylizedItem);

        //to znaci da nitko nije ulogiran, anonimni smo
        if(request.getSession().getAttribute("curUserUsername")==null) {
            shoppingCartService.spremiUKosaricu(request.getSession().getId(), stylizedItem, totalCost);
        } else {
            shoppingCartService.spremiUKosaricu((String)request.getSession().getAttribute("curUserUsername"), stylizedItem, totalCost);

        }

        return "redirect:/items";
    }
}

