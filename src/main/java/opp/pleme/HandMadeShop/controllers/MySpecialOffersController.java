package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.AcceptPriceForm;
import opp.pleme.HandMadeShop.forms.OfferPriceForm;
import opp.pleme.HandMadeShop.model.*;
import opp.pleme.HandMadeShop.repositories.*;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import opp.pleme.HandMadeShop.service.SpecialOfferService;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/mySpecialOffers")
public class MySpecialOffersController {

    @Autowired
    private RequestedSpecialOfferRepository requestedSpecialOfferRepository;
    @Autowired
    private UserService userService;
    @Autowired
    private SpecialOfferService specialOfferService;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private NapkinRepository napkinRepository;
    @Autowired
    private StyleRepository styleRepository;
    @Autowired
    private ItemRepository itemRepository;

    @GetMapping("")
    public String getForm(Model model, HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null
                || ((int) request.getSession().getAttribute("curUserLevelOfAuthority") != 1)) {
            return "redirect:/";
        }
        model.addAttribute("requestedSpecialOffers", requestedSpecialOfferRepository.findAll());
        return "mySpecialOffers";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String acceptPrice(@RequestParam("method") String method, HttpServletRequest request, Model model) {

        model.addAttribute("requestedSpecialOffers", requestedSpecialOfferRepository.findAll());
        AcceptPriceForm apf = new AcceptPriceForm();
        apf.popuniFormIzHttp(request);


        Optional<SpecialOffer> specialOffer = requestedSpecialOfferRepository.findById(Long.valueOf(apf.getSpecialOfferId()));

        String username = (String) request.getSession().getAttribute("curUserUsername");
        StylizedItem stylizedItem = null;

        if (specialOffer.isPresent()) {
            stylizedItem = new StylizedItem();
            stylizedItem.setPriceOfStylizedItem(specialOffer.get().getPrice());
            stylizedItem.setOpis(specialOffer.get().getSpecification());

            stylizedItem.setNapkin(napkinRepository.findByName("Narucena salveta"));
            stylizedItem.setStyle(styleRepository.findByName("Naruceni stil"));
            stylizedItem.setItem(itemRepository.findByName("Naruceni predmet"));
            stylizedItem.getItem().setItemPrice(stylizedItem.getPriceOfStylizedItem());
        }
        if (method.equals("reject")) {
            specialOfferService.delete(requestedSpecialOfferRepository.findById
                    (Long.valueOf(apf.getSpecialOfferId())).orElse(null));
            return "redirect:/mySpecialOffers";
        } else if (method.equals("accept")) {
            userService.prihvatiCijenu(apf);
            shoppingCartService.spremiUKosaricu((String)request.getSession().getAttribute("curUserUsername"), stylizedItem, stylizedItem.getPriceOfStylizedItem());

            return "mySpecialOffers";
        }

        return "mySpecialOffers";
    }
}

