package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.AcceptStoryForm;
import opp.pleme.HandMadeShop.model.Comment;
import opp.pleme.HandMadeShop.model.Story;
import opp.pleme.HandMadeShop.repositories.StoryRepository;
import opp.pleme.HandMadeShop.service.StoryService;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/suggestedStories")
public class SuggestedStoriesController {

    private StoryRepository storyRepository;

    @Autowired
    private UserService userService;
    @Autowired
    private StoryService storyService;

    public SuggestedStoriesController(StoryRepository storyRepository) {
        this.storyRepository = storyRepository;
    }

    @GetMapping("")
    public String getForm(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("curUserId") == null
                || ((int) request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }

        boolean approved = true;
        model.addAttribute("suggestedStories", storyRepository.findAll());
        Map<Story, List<Comment>> mapa = new HashMap<>();
        for (Story story : storyRepository.findAll()) {
            mapa.put(story, story.getComments());
        }

        for (Story story : storyRepository.findAll()) {
            if (!story.isApproved()) {
                approved = false;
                break;
            }
        }

        model.addAttribute("storiesCommentsMap", mapa);
        model.addAttribute("noSuggestedStories", approved);

        return "suggestedStories";
    }

    @RequestMapping(value = "", method = RequestMethod.POST)
    public String approveStory(@RequestParam("method") String method, HttpServletRequest request, Model model) {

        model.addAttribute("suggestedStories", storyRepository.findAll());

        AcceptStoryForm asf = new AcceptStoryForm();
        asf.popuniFormIzHttp(request);

        if (method.equals("reject")) {
            storyService.delete(storyRepository.findById
                    (Long.valueOf(asf.getSuggestedStoryId())).orElse(null));
            return "redirect:/stories";
        } else if (method.equals("accept")) {
            userService.prihvatiPricu(asf);
            return "redirect:/stories";
        }

        return "stories";
    }
}