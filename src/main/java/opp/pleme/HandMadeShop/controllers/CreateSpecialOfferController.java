package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.CreateSpecialOfferForm;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/createSpecialOffer")
public class CreateSpecialOfferController {

    @Autowired
    private UserService userService;

    @GetMapping("")
    public String getForm(HttpServletRequest request) {
        if (request.getSession().getAttribute("curUserId") == null
                || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 1)) {
            return "redirect:/";
        }

        return "createSpecialOffer";
    }

    @RequestMapping
    public String createSO(@RequestParam("method") String method, HttpServletRequest request, Model model) {
        CreateSpecialOfferForm sof = new CreateSpecialOfferForm();
        sof.popuniFormIzHttp(request);

        userService.naruciUkrasavanje(sof);
        return "redirect:/mySpecialOffers";
    }

}
