package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.CommentForm;
import opp.pleme.HandMadeShop.repositories.CommentRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
public class CommentController {

    @Autowired
    private UserService userService;

    @ModelAttribute("commentForm")
    public CommentForm populateFeatures() {
        return new CommentForm();
    }

    @GetMapping("")
    public String getForm() {
        return "index";
    }

    @RequestMapping(value="/stories", method = RequestMethod.POST)
    public String commentStory(@RequestParam("method") String method, HttpServletRequest request, Model model) {

        if (!method.equals("save")) {
            return "redirect:/";
        }

        CommentForm cf = new CommentForm();
        cf.popuniFormIzHttp(request);

        userService.komentirajPricuFormom(cf);

        return "redirect:/stories";
    }

}

