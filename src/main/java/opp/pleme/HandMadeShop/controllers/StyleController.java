package opp.pleme.HandMadeShop.controllers;


import opp.pleme.HandMadeShop.forms.StyleForm;
import opp.pleme.HandMadeShop.repositories.StyleRepository;
import opp.pleme.HandMadeShop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class StyleController {
    @Autowired
    private StyleRepository styleRepository;
    @Autowired
    private UserService userService;

    public StyleController(StyleRepository styleRepository, UserService userService) {
        this.styleRepository = styleRepository;
        this.userService = userService;
    }

    @GetMapping("/styles")
    public String getStyles(Model model, HttpServletRequest request) {

        if (request.getSession().getAttribute("curUserId") == null
                || ((int)request.getSession().getAttribute("curUserLevelOfAuthority") != 2)) {
            return "redirect:/";
        }
        model.addAttribute("styles", styleRepository.findAll());
        return "styles";
    }

    @RequestMapping(value = "/styles", method = RequestMethod.POST)
    public String addStyle(@RequestParam("method") String method, HttpServletRequest request, Model model) {
        if (!method.equals("save")) {
            return "redirect:/styles";
        }
        StyleForm styleForm = new StyleForm();
        styleForm.popuniStyleFormIzHttpa(request);

        userService.dodajStil(styleForm);
        return "redirect:/styles";
    }
}

