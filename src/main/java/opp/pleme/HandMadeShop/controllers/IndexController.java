package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.repositories.ShoppingCartRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
public class IndexController {

    @Autowired
    private ShoppingCartRepository shoppingCartRepository;

    @RequestMapping({"", "/", "index", "index.html"})
    public String showIndex(HttpServletRequest request){

        //request.getSession();
        if(request.getSession().getAttribute("curUserUsername")==null) {

            if (!shoppingCartRepository.findByUsername(request.getSession().getId()).isPresent()) {
                ShoppingCart shoppingCart = new ShoppingCart();
                shoppingCart.setUsername(request.getSession().getId());
                shoppingCartRepository.save(shoppingCart);
            }

        }
        return "index";
    }
}
