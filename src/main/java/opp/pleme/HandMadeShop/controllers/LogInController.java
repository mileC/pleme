package opp.pleme.HandMadeShop.controllers;

import opp.pleme.HandMadeShop.forms.LogInForm;
import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.User;
import opp.pleme.HandMadeShop.repositories.ShoppingCartRepository;
import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import opp.pleme.HandMadeShop.repositories.UserRepository;
import opp.pleme.HandMadeShop.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;

@Controller
public class LogInController {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ShoppingCartRepository shoppingCartRepository;
    @Autowired
    private ShoppingCartService shoppingCartService;
    @Autowired
    private StylizedItemRepository stylizedItemRepository;


    @ModelAttribute("loginform")
    public LogInForm populateFeatures() {
        return new LogInForm();
    }

    @GetMapping("/login")
    public String showLogIn() {
        return "login";
    }

    @RequestMapping("/login")
    public String registerUser(@RequestParam("method") String method, final User user, final BindingResult bindingResult,
                               Model model, HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        LogInForm lf = new LogInForm();
        lf.popuniIzHttp(request);

        Optional<User> us = userRepository.findByUsernameAndPassword(lf.getUsername(), lf.getPassword());
        if (!us.isPresent()) {
            lf = new LogInForm();
            lf.setError("password", "Username or password is incorrect!");
            model.addAttribute("loginform", lf);
            return "login";
        }
        if (us.get().isBlocked()) {
            return "deniedAccess";
        }

        User userReg = us.get();

        if(shoppingCartRepository.findByUsername(request.getSession().getId()).isPresent()){
            ShoppingCart shoppingCart=shoppingCartRepository.findByUsername(request.getSession().getId()).get();
            stylizedItemRepository.deleteAllByShoppingCart(shoppingCart);
            shoppingCartService.delete(shoppingCart);
        }
        request.changeSessionId();

        request.getSession().setAttribute("curUserId", userReg.getId());
        request.getSession().setAttribute("curUserUsername", userReg.getUsername());
        request.getSession().setAttribute("curUserName", userReg.getName());
        request.getSession().setAttribute("curUserSurname", userReg.getSurname());
        request.getSession().setAttribute("curUserLevelOfAuthority", userReg.getLevelOfAuthority());
        request.getSession().setAttribute("curUserAddress", userReg.getAddress());
        request.getSession().setAttribute("curUserCardNumber", userReg.getCardNumber());
        request.getSession().setAttribute("curUserBlockedBool", userReg.isBlocked());
        request.getSession().setAttribute("curUserContact", userReg.getContact());
        request.getSession().setAttribute("curUserPassword", userReg.getPassword());

        if(!shoppingCartRepository.findByUsername((String)request.getSession().getAttribute("curUserUsername")).isPresent()){
            ShoppingCart shoppingCart = new ShoppingCart();
            shoppingCart.setUsername((String)request.getSession().getAttribute("curUserUsername"));
            shoppingCartRepository.save(shoppingCart);
        }

        model.addAttribute("loginform", new LogInForm());
        redirectAttributes.addFlashAttribute("flashuser", userReg);

        return "redirect:/index";
    }
}
