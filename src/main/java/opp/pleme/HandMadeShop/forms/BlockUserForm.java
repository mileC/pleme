package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;

public class BlockUserForm {

    private String userId;

    public String getUserId() {
        return userId;
    }

    private String convert(String s) {
        if (s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniFormIzHttp(HttpServletRequest request) {
        this.userId= request.getParameter("userId");
    }


}