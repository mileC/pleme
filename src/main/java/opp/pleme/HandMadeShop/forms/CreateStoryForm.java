package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class CreateStoryForm {
    Map<String, String> errors = new HashMap<>();

    private String title;
    private String video;
    private String picture;
    private String text;

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getTitle() {
        return title;
    }

    public String getVideo() {
        return video;
    }

    public String getPicture() {
        return picture;
    }

    public String getText() {
        return text;
    }

    private String convert(String s) {
        if (s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniRegFormIzHttp(HttpServletRequest request) {
        this.title = request.getParameter("title");

        if (request.getParameter("video").trim().isEmpty()) {
            this.video = null;
        } else {
            this.video = request.getParameter("video");
        }

        if (request.getParameter("picture").trim().isEmpty()) {
            this.picture = null;
        } else {
            this.picture = request.getParameter("picture");
        }

        this.text = request.getParameter("text");
    }

    public void validate() {
        if (title.equals(null)) {
            String e = "Please enter a valid title.";
            saveInMap(text, e);
        }

        if (text.equals(null)) {
            String e = "Please enter story content.";
            saveInMap(text, e);
        }
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    private void saveInMap(String k, String v) {
        errors.put(k, v);
        //TO DO
    }

}