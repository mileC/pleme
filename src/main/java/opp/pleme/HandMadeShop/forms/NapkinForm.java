package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;

public class NapkinForm {
    private String opis;

    public NapkinForm(){

    }

    public String getOpis() {
        return opis;
    }

    public void setOpis(String opis) {
        this.opis = opis;
    }

    public void popuniNapkinFormIzHttp(HttpServletRequest request){
        this.opis=request.getParameter("name");
    }

}
