package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;

public class AcceptStoryForm {

    private String suggestedStoryId;

    public String getSuggestedStoryId() {
        return suggestedStoryId;
    }

    private String convert(String s) {
        if (s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniFormIzHttp(HttpServletRequest request) {
        this.suggestedStoryId = request.getParameter("suggestedStoryId");
    }


}