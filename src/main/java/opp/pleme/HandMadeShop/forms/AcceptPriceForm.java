package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class AcceptPriceForm {

    private String specialOfferId;

    public String getSpecialOfferId() {
        return specialOfferId;
    }

    private String convert(String s) {
        if (s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniFormIzHttp(HttpServletRequest request) {
        this.specialOfferId = request.getParameter("specialOfferId");
    }


}