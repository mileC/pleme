package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;

public class StyleForm {
    private String name;
    private String description;
    private float coefficient;

    public StyleForm(){}

    public void popuniStyleFormIzHttpa(HttpServletRequest request){
        this.name=request.getParameter("name");
        this.description=request.getParameter("description");
        this.coefficient= Float.parseFloat(request.getParameter("coefficient"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getCoefficient() {
        return coefficient;
    }

    public void setCoefficient(float coefficient) {
        this.coefficient = coefficient;
    }
}
