package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;

public class PaymentAnonForm {
    private String username;
    private String name;
    private String surname;
    private String address;
    private String contact;
    private String cardNumber;

    public void popuniFormIzHttpa(HttpServletRequest request) {
        this.username=request.getSession().getId();
        this.name = request.getParameter("name");
        this.surname = request.getParameter("surname");
        this.address = request.getParameter("address");
        this.contact = request.getParameter("contact");
        this.cardNumber = request.getParameter("cardNumber");
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
