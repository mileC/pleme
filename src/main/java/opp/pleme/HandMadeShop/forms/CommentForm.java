package opp.pleme.HandMadeShop.forms;

import opp.pleme.HandMadeShop.repositories.UserRepository;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class CommentForm {

    private String storyId;
    private String userId;
    private String text;
    private String anonymousComment;

    private UserRepository userRepository;

    private Map<String, List<String>> errors = new HashMap<>();

    public CommentForm() {
    }

    public List<String> getError(String name) {
        return errors.get(name);
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setError(String key, String desc) {
        if (errors.containsKey(key)) {
            errors.get(key).add(desc);
        } else {
            List<String> list = new LinkedList<>();
            list.add(desc);
            errors.put(key, list);
        }
    }

    public String getStoryId() {
        return storyId;
    }

    public String getUserId() {
        return userId;
    }

    public String getText() {
        return text;
    }

    public String getAnonymous() {
        return anonymousComment;
    }

    public void popuniFormIzHttp(HttpServletRequest request) {
        this.storyId = request.getParameter("storyId");
        this.userId = request.getParameter("userId");
        this.text = request.getParameter("comment");
        this.anonymousComment = request.getParameter("anonymousComment");
    }

    public void validate() {
        errors.clear();
        if (text.isEmpty()) {
            setError("comment", "Please enter a valid comment.");
        }
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    private void saveInMap(String k, List<String> v) {
        errors.put(k, v);
    }


}