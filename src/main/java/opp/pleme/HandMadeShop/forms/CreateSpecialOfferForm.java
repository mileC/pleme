package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class CreateSpecialOfferForm {
    Map<String, String> errors = new HashMap<>();

    private String userId;
    private String specification;
    private String mediaLink;

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getUserId() {
        return userId;
    }

    public String getSpecification() {
        return specification;
    }

    public String getMediaLink() {
        return mediaLink;
    }


    private String convert(String s){
        if(s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniFormIzHttp(HttpServletRequest request){
        this.userId=request.getParameter("userId");
        this.specification=request.getParameter("specification");
        this.mediaLink=request.getParameter("medialink");
    }

    public void validate(){
        if (specification.equals(null)) {
            String e = "Please enter a valid title.";
            saveInMap(specification, e);
        }
        if(!mediaLink.contains("www") ){
            String e="Please enter a valid link format.";
            saveInMap(mediaLink, e);
        }

    }

    public boolean isValid(){
        return errors.isEmpty();
    }
    private void saveInMap(String k, String v){
        errors.put(k,v);
        //TO DO
    }

}