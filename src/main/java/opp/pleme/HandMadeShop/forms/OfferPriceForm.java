package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class OfferPriceForm {
    Map<String, String> errors = new HashMap<>();

    private String specialOfferId;
    private String price;

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getSpecialOfferId() {
        return specialOfferId;
    }

    public String getPrice() {
        return price;
    }

    private String convert(String s) {
        if (s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniFormIzHttp(HttpServletRequest request) {
        this.specialOfferId = request.getParameter("specialOfferId");
        this.price = request.getParameter("price");
    }

    public void validate() {
        //TODO
    }

    public boolean isValid() {
        return errors.isEmpty();
    }

    private void saveInMap(String k, String v) {
        errors.put(k, v);
        //TO DO
    }

}