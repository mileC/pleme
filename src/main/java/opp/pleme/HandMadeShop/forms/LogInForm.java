package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class LogInForm {

    private String username;
    private String password;
    Map<String, List<String>> errors = new HashMap<>();

    public LogInForm(){}

    public void popuniIzHttp(HttpServletRequest request){
        this.username=request.getParameter("username");
        this.password=request.getParameter("password");
    }

    public List<String> getError(String name){
        return errors.get(name);
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setError(String key, String desc){
        if(errors.containsKey(key)){
            errors.get(key).add(desc);
        } else {
            List<String> list=new LinkedList<>();
            list.add(desc);
            errors.put(key, list);
        }
    }

    public boolean containsError(String name){
        return errors.containsKey(name);
    }

    public boolean isValid(){
        return errors.isEmpty();
    }

    public void validate(){

    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }


}
