package opp.pleme.HandMadeShop.forms;


import opp.pleme.HandMadeShop.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class EditProfileForm {
    private String name;
    private String surname;
    private String address;
    private String username;
    private String contact;
    private String card_number;

    private Map<String, List<String>> errors = new HashMap<>();

    public EditProfileForm(){

    }

    public List<String> getError(String name){
        return errors.get(name);
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setError(String key, String desc){
        if(errors.containsKey(key)){
            errors.get(key).add(desc);
        } else {
            List<String> list=new LinkedList<>();
            list.add(desc);
            errors.put(key, list);
        }
    }

    public void popuniRegFormIzHttp(HttpServletRequest request){
        this.name=request.getParameter("name");
        this.surname=request.getParameter("surname");
        this.address=request.getParameter("address");
        this.username=request.getParameter("username");
        this.contact=request.getParameter("contact");
        this.card_number=request.getParameter("card_number");
    }
    public boolean containsError(String name){
        return errors.containsKey(name);
    }

    public void popuniURecord(User u){
        u.setName(this.name);
        u.setSurname(this.surname);
        u.setUsername(this.username);
        u.setAddress(this.address);
        u.setCardNumber(this.card_number);
        u.setContact(this.contact);
    }

    public void validate(){
        errors.clear();
        if(containsChars(card_number)){
            setError("card_number", "Broj kartice ne smije sadržavati slova!");
        }
    }
    public boolean isValid(){
        return errors.isEmpty();
    }
    public final boolean containsChars(String s) {
        boolean containsChars = false;

        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (! Character.isDigit(c)) {
                    containsChars=true;
                    break;
                }
            }
        }

        return containsChars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getCard_number() {
        return card_number;
    }

    public void setCard_number(String card_number) {
        this.card_number = card_number;
    }
}