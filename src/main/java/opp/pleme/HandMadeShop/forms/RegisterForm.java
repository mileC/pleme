package opp.pleme.HandMadeShop.forms;

import opp.pleme.HandMadeShop.model.User;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class RegisterForm {

    private String username;
    private String name;
    private String surname;
    private String address;
    private String contact;
    private String password;
    private String card_number;

    private Map<String, List<String>> errors = new HashMap<>();

    public RegisterForm(){}

    public List<String> getError(String name){
        return errors.get(name);
    }

    public Map<String, List<String>> getErrors() {
        return errors;
    }

    public void setError(String key, String desc){
        if(errors.containsKey(key)){
            errors.get(key).add(desc);
        } else {
            List<String> list=new LinkedList<>();
            list.add(desc);
            errors.put(key, list);
        }
    }

    public boolean containsError(String name){
        return errors.containsKey(name);
    }

    public String getUsername() {
        return username;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public String getPassword() {
        return password;
    }

    public String getCardNumber() {
        return card_number;
    }

    private String convert(String s){
        if(s==null) return s;
        s.trim();
        return s;
    }


    public void popuniRegFormIzHttp(HttpServletRequest request){
        this.username=request.getParameter("username");
        this.name=request.getParameter("name");
        this.surname=request.getParameter("surname");
        this.address=request.getParameter("address");
        this.contact=request.getParameter("contact");
        this.password=request.getParameter("password");
        this.card_number=request.getParameter("card_number");
    }

    public void popuniIzRecorda(User user){
        this.name=user.getName();
        this.username=user.getUsername();
        this.surname=user.getSurname();
        this.address=user.getAddress();
        this.contact=user.getContact();
        this.password=user.getPassword();
        this.card_number=user.getCardNumber();
    }

    public void popuniURecord(User u){
        u.setName(this.name);
        u.setSurname(this.surname);
        u.setUsername(this.username);
        u.setAddress(this.address);
        u.setCardNumber(this.card_number);
        u.setContact(this.contact);
        u.setPassword(this.password);
    }

    public void validate(){
        errors.clear();

        if(name.contains("0") || name.contains("1") || name.contains("2") || name.contains("3") || name.contains("4") || name.contains("5") ||
                name.contains("6") || name.contains("7") || name.contains("8") || name.contains("9") ){
            setError("name", "Ime sadrži greške! Molimo upišite valjani oblik imena!");
        }

        if(surname.contains("0") || surname.contains("1") || surname.contains("2") || surname.contains("3") || surname.contains("4") || surname.contains("5") ||
                name.contains("6") || surname.contains("7") || surname.contains("8") || surname.contains("9") ){
            setError("surname", "Prezime sadrži greške! Molimo upišite valjani oblik prezimena!");
        }

        if(!contact.contains("@")){
            setError("contact", "Molimo upišite valjani oblik emaila npr: name@gmail.com");
        }
        if(password.length()<6){
            setError("password", "Lozinka mora biti duljine barem 6 znakova!");
        }
        if(containsChars(card_number)){
            setError("card_number", "Broj kartice ne smije sadržavati slova!");
        }


    }
    public boolean isValid(){
        return errors.isEmpty();
    }

    public final boolean containsChars(String s) {
        boolean containsChars = false;

        if (s != null && !s.isEmpty()) {
            for (char c : s.toCharArray()) {
                if (! Character.isDigit(c)) {
                    containsChars=true;
                    break;
                }
            }
        }

        return containsChars;
    }
}
