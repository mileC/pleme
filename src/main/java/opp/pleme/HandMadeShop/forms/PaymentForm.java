package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

public class PaymentForm {
    Map<String, String> errors = new HashMap<>();

    private String username;
    private String name;
    private String surname;
    private String address;
    private String contact;
    private String cardNumber;

    public Map<String, String> getErrors() {
        return errors;
    }

    public String getUsername() {
        return this.username;
    }

    public String getName() {
        return this.name;
    }

    public String getSurname() {
        return surname;
    }

    public String getAddress() {
        return address;
    }

    public String getContact() {
        return contact;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    private String convert(String s) {
        if (s.isEmpty() || s.equals(null)) return s;
        s.trim();
        return s;
    }

    public void popuniFormIzHttpa(HttpServletRequest request) {
        this.username = request.getParameter("username");
        this.name = request.getParameter("name");
        this.surname = request.getParameter("surname");
        this.address = request.getParameter("address");
        this.contact = request.getParameter("contact");
        this.cardNumber = request.getParameter("cardNumber");
    }



/*    public void validate() {
        if (title.equals(null)) {
            String e = "Please enter a valid title.";
            saveInMap(text, e);
        }

        if (text.equals(null)) {
            String e = "Please enter story content.";
            saveInMap(text, e);
        }
    }
*/

    public boolean isValid() {
        return errors.isEmpty();
    }

    private void saveInMap(String k, String v) {
        errors.put(k, v);
        //TO DO
    }

}