package opp.pleme.HandMadeShop.forms;

import javax.servlet.http.HttpServletRequest;

public class ItemsListForm {
    private String name;
    private String link;
    private String description;
    private float itemPrice;

    public ItemsListForm(){}

    public void popuniItemsListFormIzHttp(HttpServletRequest request){
        this.name=request.getParameter("name");
        this.link=request.getParameter("link");
        this.description=request.getParameter("description");
        this.itemPrice= Float.parseFloat(request.getParameter("item_price"));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public float getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(float itemPrice) {
        this.itemPrice = itemPrice;
    }
}
