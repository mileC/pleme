package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.Comment;
import org.springframework.data.repository.CrudRepository;

public interface CommentRepository extends CrudRepository<Comment, Long> {
}
