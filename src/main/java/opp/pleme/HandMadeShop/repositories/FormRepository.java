package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.Form;
import org.springframework.data.repository.CrudRepository;

public interface FormRepository extends CrudRepository<Form, Long> {
}
