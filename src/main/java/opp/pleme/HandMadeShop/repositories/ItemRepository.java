package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.Item;
import org.springframework.data.repository.CrudRepository;

import java.util.function.LongFunction;

public interface ItemRepository extends CrudRepository<Item, Long> {
    Item findByName(String name);

}
