package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.User;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {
    Optional<User> findByContact(String contact);

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameAndPassword(String username, String password);

    int countByUsername(String username);

    int countByContact(String contact);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User u set  u.address = :address where u.username = :username")
    int updateUserSetAddressForIdNative(@Param("address") String address, @Param("username") String username);

    @Transactional
    @Modifying(clearAutomatically = true)
    @Query("update User u set u.cardNumber= :card_number where u.username= :username")
    int updateUserSetCardnumber(@Param("card_number") String card_number, @Param("username") String username);

    @Transactional
    @Modifying
    @Query("update User u set u.wallet= :wallet where u.username= :username")
    int updateUserSetWallet(@Param("wallet") float wallet, @Param("username") String username);
}