package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.SpecialOffer;
import org.springframework.data.repository.CrudRepository;

public interface SpecialOfferRepository extends CrudRepository<SpecialOffer, Long> {

}
