package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.Style;
import org.springframework.data.repository.CrudRepository;

public interface StyleRepository extends CrudRepository <Style, Long> {
    Style findByName(String name);

}
