package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.Napkin;
import org.springframework.data.repository.CrudRepository;

public interface NapkinRepository extends CrudRepository<Napkin, Long> {
    Napkin findByName(String name);
}
