package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.ShoppingCart;
import opp.pleme.HandMadeShop.model.StylizedItem;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

public interface StylizedItemRepository extends CrudRepository<StylizedItem, Long> {

    @Modifying
    @Transactional
    @Query("delete from StylizedItem stItem where stItem.shoppingCart =: shoppingCart_id")
    void deleteByShoppingCartId(@Param("shoppingCart_id") ShoppingCart shoppingCart_id);

    @Transactional
    @Modifying
    void deleteAllByShoppingCart(ShoppingCart shoppingCart);
}
