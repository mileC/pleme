package opp.pleme.HandMadeShop.repositories;

import opp.pleme.HandMadeShop.model.Comment;
import opp.pleme.HandMadeShop.model.Story;
import org.springframework.data.repository.CrudRepository;

import java.util.Set;

public interface StoryRepository extends CrudRepository<Story, Long> {

}
