package opp.pleme.HandMadeShop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HandMadeShopApplication {

    public static void main(String[] args) {

        SpringApplication.run(HandMadeShopApplication.class, args);

    }

}

