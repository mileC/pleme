package opp.pleme.HandMadeShop.bootstrap;

import opp.pleme.HandMadeShop.model.*;
import opp.pleme.HandMadeShop.repositories.StylizedItemRepository;
import opp.pleme.HandMadeShop.repositories.TransactionsRepository;
import opp.pleme.HandMadeShop.service.*;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Collections;

@Component
public class DataLoader implements ApplicationListener<ContextRefreshedEvent> {

    private final UserService userService;
    private final ItemService itemService;
    private final CommentService commentService;
    private final FormService formService;
    private final NapkinService napkinService;
    private final ShoppingCartService shoppingCartService;
    private final SpecialOfferService specialOfferService;
    private final StoryService storyService;
    private final StyleService styleService;
    private final StylizedItemService stylizedItemService;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        try {
            initData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DataLoader(UserService userService, ItemService itemService, CommentService commentService, FormService formService, NapkinService napkinService, ShoppingCartService shoppingCartService, SpecialOfferService specialOfferService, StoryService storyService, StyleService styleService, StylizedItemService stylizedItemService) {
        this.userService = userService;
        this.itemService = itemService;
        this.commentService = commentService;
        this.formService = formService;
        this.napkinService = napkinService;
        this.shoppingCartService = shoppingCartService;
        this.specialOfferService = specialOfferService;
        this.storyService = storyService;
        this.styleService = styleService;
        this.stylizedItemService = stylizedItemService;
    }

    public void initData() throws Exception {

        User admin = new User("admin", "Ružica", "Ruž", "Gorski korar", "admin@gmail.com", "administrator", "17888852369440", false, 2);
        User anonymous = new User("anonymous", "nepo", "znatić");
        anonymous.setId(0L);
        User an = new User("anonymous23");
        User AN2 = new User("anonymous332");
        userService.save(admin);
        userService.save(anonymous);
        userService.save(an);
        userService.save(AN2);

        User filip = new User("razbijac", "Filip", "Katušić", "Zagreb", "admin1@gmail.com", "nemoze", "964519819", false, 2);
        User ante = new User("Sinjanin", "Ante", "Balic...", "Sinj 123", "name27@gmail.com", "SINJSINJ", "019184161", false, 1);
        userService.save(filip);
        userService.registrirajSe(ante);

        User granny = new User("granny", "Ružica", "Hrgović", "Gospić", "name20@gmail.com", "lozinka5", "158820463", false, 1);
        User decoupageFan = new User("decoupageFan", "Ana", "Anić", "Zagreb", "name21@gmail.com", "lozinka5", "046355950", false, 1);
        userService.registrirajSe(granny);
        userService.registrirajSe(decoupageFan);

        Story pricaODecoupageu = new Story("U ovom tekstu opisat ćemo osnovni princip rada sa salvetama nakon čega " +
                "se i sami možete okušati u ovoj zahvalnoj tehnici. Sigurni smo da ćete biti oduševljeni rezultatima!" +
                "Potrebni materijali: decoupage lak ljepilo (sjajno ili mat), " +
                "prozirni lak za završni premaz (po želji), crackle lack – posebni lak za raspucani efekt, " +
                "salvete (tanke 2- ili 3-slojne) različitih motiva i boja i akrilne boje. " +
                "Predmeti koji se obično ukrašavaju: PVC kugle i drugi PVC gotovi oblici, drveni predmeti, kutije, " +
                "terakota posude, teglice, svijećnjaci, svijeće, staklene posude, čaše ili posude od porculana, oblici " +
                "i gotove forme od stiropora, lanterna folije i sl.");
        pricaODecoupageu.setTitle("Decoupage");
        pricaODecoupageu.setPicture("https://usercontent1.hubstatic.com/13536480_f520.jpg");
        pricaODecoupageu.setVideo("https://www.youtube.com/watch?v=ew1sGPdyhnU");
        pricaODecoupageu.setApproved(true);
        storyService.save(pricaODecoupageu);

        Story pricaONovojSalveti = new Story("Napokon!!! U ponudu nam stiže najnovija salveta sa uzorkom suncokreta" +
                " po uzoru na Van Goghove suncokrete. Ova divna salveta se može naručiti u našoj standardnoj " +
                "ponudi po promotivnoj cijeni od 12.00 kn. Savršena za ukrašavanje svih predmeta, garantiramo " +
                "da ćete biti ZADOVOLJNI ukoliko se odlučite za ovu salvetu.");
        pricaONovojSalveti.setTitle("SUNCOKRETNA SALVETA");
        pricaONovojSalveti.setPicture("http://www.mustash.ro/2645-large_default/servetel-pentru-decoupage-ihr-l435700-van-gogh-sunflowers.jpg");
        pricaONovojSalveti.setVideo("https://www.youtube.com/watch?v=vp5qJlr4go0");
        pricaONovojSalveti.setApproved(true);
        storyService.save(pricaONovojSalveti);


        userService.komentirajPricu(granny, "Dobra večer prijatelji!", pricaODecoupageu);
        userService.komentirajPricu(AN2, "dobra večer i tebi prijateljice", pricaODecoupageu);
        userService.komentirajPricu(decoupageFan, "Otkad sam otkrila decoupage, moj život je ponovno dobioo smisao!", pricaODecoupageu);
        userService.komentirajPricu(an, "jako zanimljiva tehnika, sviđa mi se!", pricaODecoupageu);

        Napkin napkin = new Napkin("retro");
        Napkin napkin2 = new Napkin("moderno");
        Napkin napkin3 = new Napkin("cvjetni uzorak");
        Napkin napkin4 = new Napkin("Narucena salveta");
        Napkin napkin5 = new Napkin("Suncokret salveta");
        napkinService.save(napkin);
        napkinService.save(napkin2);
        napkinService.save(napkin3);
        napkinService.save(napkin4);
        napkinService.save(napkin5);

        Style naruceniStil = new Style("Naruceni stil");
        Style retroStil = new Style("Retro", "Retro stil", 1.5f);
        Style moderniStil = new Style("Moderno", "Moderno!", 1.2f);
        Style gitak = new Style("Super kul urbani stil", "Gitak TV inspired", 10.5f);
        styleService.save(naruceniStil);
        styleService.save(retroStil);
        styleService.save(moderniStil);
        styleService.save(gitak);

        Item nocniOrmaric = new Item("NOĆNI ORMARIĆ", "https://i.pinimg.com/564x/92/8d/1f/928d1fedfde21e4a86373de4c53d0faa.jpg", "Predivan noćni ormarić idealan za vašu spavaću sobu.\nDimenzije: 90x50x40 [cm]", 150);
        Item zaFilipa = new Item("PALICA", "https://shop.r10s.jp/stay246/cabinet/supreme8/g2v3062.jpg", "Budite šik uz decopuge palicu!\n Duljina: 1,1m ", 90);
        Item boca = new Item("BOCA", "http://giftstation.in/wp-content/uploads/2017/09/Yellow-Sparrow-decoupage-bottle.jpg", "Jedinstvena boca za vaše omiljeno piće!\nVisina boce: 22cm, \nZapremnina: 0,75l", 29.99f);
        Item naruceni = new Item("Naruceni predmet", "Predmet prema vasoj zelji! ", "Odaberite opciju naruci ukrasavanje u meniju!", 1);
        Item violina = new Item("VIOLINA", "http://www.violina-prima.com/img/violina%20koncertna.jpg", "4 žice, sviraju ju cigani. \n\nDimenzije: 14'' (duljina tijela)", 700);
        Item sat = new Item("SAT", "https://5.imimg.com/data5/KB/WC/MY-24402558/pendulum-wall-clock-500x500.jpg", "Zidni sat sa njihalicom.\nDimenzije: 20x50x15 [cm]", 200);
        Item item = new Item("KUTIJA", "http://4.bp.blogspot.com/-64vp-rn0xHQ/UuPju_c6JRI/AAAAAAAACqA/YMV_19b-6Yk/s1600/DSCN5852.JPG", "Vintage kutija, idealna za spremanje nakita.\nDimenzije: 30x40x20 [cm]", 20);
        itemService.save(nocniOrmaric);
        itemService.save(zaFilipa);
        itemService.save(boca);
        itemService.save(naruceni);
        itemService.save(violina);
        itemService.save(sat);
        itemService.save(item);

        Story prica3 = new Story("prva prica");
        prica3.setTitle("Saluti da Venezia");
        prica3.setText("Lorem Ipsum is simply dummy text of the printing and typesetting industry. " +
                "Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer " +
                "took a galley of type and scrambled it to make a type specimen book. It has survived not only " +
                "five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was " +
                "popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and " +
                "more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        prica3.setPicture("http://www.pilloledistoria.it/wp-content/uploads/2018/08/capodanno-a-venezia.jpeg");
        prica3.setApproved(true);
        storyService.save(prica3);

        Story prica1 = new Story("Inače ne pišem ovakve priče, ali teta Ana me izula iz šlapa. Njene ruke su " +
                "zlatne. Bio sam malo skeptičan na početku uzevši u obzir koliko je zahtjevno ukrasiti protezu za zube. " +
                "My tooth is decorated. Thank you teta Ana! :))) <3 *_* XOXOXOXOXOXOXO");
        prica1.setTitle("Teta Ana, volim vas!");
        prica1.setPicture("https://tse4.mm.bing.net/th?id=OIP.7I2HyARybkw_-XglmUdzzgHaG4&pid=Api&w=1296&h=1205&rs=1&p=0");
        prica1.setApproved(true);
        storyService.save(prica1);

        Story prica = new Story("Nikada nisam pensala da more jena zagrebačka žena tako da kapi primorsko sarce." +
                " Aš je zato ča ja moran udelati ovu priču. Gospojo, ma vi ste više od gospoje. Vi ste jedan čovik. Ma ne pensan zaspravlje. " +
                "Ma se tako reče  ovdeka kod nas va Kostrene. Ča je život vengo fantažija z vašin rukami. Vajka ću van biti" +
                "zahvalna. Ala šu, trdo sprida meko pozadi. Čakulat ćemo in persona. bacci e abbracci :*");
        prica.setTitle("KOŠTRENJANSKA FANTAZIJA");
        prica.setPicture("https://cff2.earth.com/uploads/2017/02/02174839/New-app-helps-fisherman-seek-and-sell-their-catch-.jpg");
        prica.setVideo("https://www.youtube.com/watch?v=BJAZHqhwui0");
        prica.setApproved(true);
        storyService.save(prica);


        userService.registrirajSe(new User("snejk", "Ardita", "Šalja", "Rijeka", "name1@gmail.com", "zmija123", "985234712", false, 1));
        userService.registrirajSe(new User("mino.najiaci", "Jakov", "Vidulić", "Lošinj", "name2@gmail.com", "iaco555", "114551545", false, 1));
        userService.registrirajSe(new User("milela", "Mirela", "Vučić", "Sinj", "name3@gmail.com", "auu1950", "153214775", false, 1));
        userService.registrirajSe(new User("punjac", "Mile", "Čarić", "Dicmo", "name4@gmail.com", "puknut7", "78756900", false, 1));
        userService.registrirajSe(new User("trener", "Slaven", "Bilić", "Split", "name5@gmail.com", "westham1", "965651950", false, 1));
        userService.registrirajSe(new User("bakica", "Anka", "Filipović", "Imocki", "name6@gmail.com", "lozinka5", "100258852", false, 1));
        userService.registrirajSe(new User("krešo", "Krešo", "Bengalka", "Split", "name7@gmail.com", "lozinka5", "156355950", false, 1));
        userService.registrirajSe(new User("žuvi", "Žuvi", "Žuvela", "Split", "name8@gmail.com", "lozinka5", "158820950", false, 1));
        userService.registrirajSe(new User("uk21", "Ana", "Anić", "Zagreb", "name9@gmail.com", "lozinka5", "158820460", false, 1));
        userService.registrirajSe(new User("laž", "Ana", "Anić", "Zagreb", "name10@gmail.com", "lozinka5", "158820463", false, 1));
        userService.registrirajSe(new User("random", "Ana", "Anić", "Zagreb", "name11@gmail.com", "lozinka5", "158820950", false, 1));
        userService.registrirajSe(new User("boring", "Ana", "Anić", "Zagreb", "name12@gmail.com", "lozinka5", "158820950", false, 1));
        userService.registrirajSe(new User("jelen", "Ana", "Anić", "Zagreb", "name13@gmail.com", "lozinka5", "158820450", false, 1));
        userService.registrirajSe(new User("papagaj", "Ana", "Anić", "Zagreb", "name14@gmail.com", "lozinka5", "158855950", false, 1));
        userService.registrirajSe(new User("lost", "Ana", "Anić", "Zagreb", "name15@gmail.com", "lozinka5", "158820460", false, 1));
        userService.registrirajSe(new User("notFound", "Ana", "Anić", "Zagreb", "name16@gmail.com", "lozinka5", "158855950", false, 1));
        userService.registrirajSe(new User("zadnji", "Ana", "Anić", "Zagreb", "name17@gmail.com", "lozinka5", "158820950", false, 1));

    }
}
