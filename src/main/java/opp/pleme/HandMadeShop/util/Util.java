package opp.pleme.HandMadeShop.util;

import opp.pleme.HandMadeShop.model.User;

import javax.servlet.http.HttpServletRequest;

public class Util {
    public static void setSessionAttributes(HttpServletRequest request, User user){
        request.getSession().setAttribute("curUserId", user.getId());
        request.getSession().setAttribute("curUserUsername", user.getUsername());
        request.getSession().setAttribute("curUserName", user.getName());
        request.getSession().setAttribute("curUserSurname", user.getSurname());
        request.getSession().setAttribute("curUserLevelOfAuthority", user.getLevelOfAuthority());
        request.getSession().setAttribute("curUserAddress", user.getAddress());
        request.getSession().setAttribute("curUserCardNumber", user.getCardNumber());
        request.getSession().setAttribute("curUserBlockedBool", user.isBlocked());
        request.getSession().setAttribute("curUserContact", user.getContact());
        request.getSession().setAttribute("curUserPassword", user.getPassword());
    }
}